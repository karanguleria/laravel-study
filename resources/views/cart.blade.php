@extends('layouts.master')

@section('content')
<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Shopping Cart</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">Cart</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Cart Area =================-->
    <section class="cart_area">
        <div class="container">
            <div>
                @if(session()->has('success_message'))
                    <div class="alert alert-success">
                        {{ session()->get('success_message') }}
                    </div>
                @endif()
            </div>
            <div>
                @if( count($errors) > 0 )
                    <div class="alert alert-success">
                        <ul> 
                            @foreach($errors->all() as $error)  
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif()
            </div>
            <div class="cart_inner">
            @if(Cart::count() > 0)
               <h3>{{Cart::count()}} Item(s) in cart </h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Product</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach(Cart::content() as $item)
                                    <tr>
                                        <td>
                                            <div class="media">
                                                <div class="d-flex">
                                                    <a href="{{route('shop.show',$item->model->slug)}}"><img src="img/cart.jpg" alt="{{ $item->model->name }}"></a>
                                                </div>
                                                <div class="media-body">
                                                    <p><a href="{{route('shop.show',$item->model->slug)}}">{{ $item->model->name }}</a></p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h5>{{ $item->model->presentPrice() }}</h5>
                                        </td>
                                        <td>
                                            <div class="product_count">
                                                <input type="text" name="qty" id="sst" maxlength="12" value="1" title="Quantity:"
                                                    class="input-text qty">
                                                <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
                                                    class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
                                                <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
                                                    class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button>
                                                    <form  method="POST" action="{{ route('cart.destroy',$item->rowId ) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit">Remove</button>
                                                    </form><br>
                                                    <form  method="POST" action="{{ route('cart.saveForLater',$item->rowId ) }}">
                                                        @csrf
                                                        <button type="submit">Save For later</button>
                                                    </form>
                                            </div>
                                        </td>
                                        <td>
                                            <h5>{{$item->model->presentPrice() }}</h5>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr class="bottom_button">
                                    <td>
                                        <a class="gray_btn" href="#">Update Cart</a>
                                    </td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <div class="cupon_text d-flex align-items-center">
                                            <input type="text" placeholder="Coupon Code">
                                            <a class="primary-btn" href="#">Apply</a>
                                            <a class="gray_btn" href="#">Close Coupon</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <h5>Subtotal</h5>
                                    </td>
                                    <td>
                                        <h5>{{ Cart::subtotal() }}</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <h5>Tax</h5>
                                    </td>
                                    <td>
                                        <h5>{{ Cart::tax() }}</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <h5>Total</h5>
                                    </td>
                                    <td>
                                        <h5>{{ Cart::total() }}</h5>
                                    </td>
                                </tr>
                                <tr class="shipping_area">
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <h5>Shipping</h5>
                                    </td>
                                    <td>
                                        <div class="shipping_box">
                                            <ul class="list">
                                                <li><a href="#">Flat Rate: $5.00</a></li>
                                                <li><a href="#">Free Shipping</a></li>
                                                <li><a href="#">Flat Rate: $10.00</a></li>
                                                <li class="active"><a href="#">Local Delivery: $2.00</a></li>
                                            </ul>
                                            <h6>Calculate Shipping <i class="fa fa-caret-down" aria-hidden="true"></i></h6>
                                            <select class="shipping_select">
                                                <option value="1">Bangladesh</option>
                                                <option value="2">India</option>
                                                <option value="4">Pakistan</option>
                                            </select>
                                            <select class="shipping_select">
                                                <option value="1">Select a State</option>
                                                <option value="2">Select a State</option>
                                                <option value="4">Select a State</option>
                                            </select>
                                            <input type="text" placeholder="Postcode/Zipcode">
                                            <a class="gray_btn" href="#">Update Details</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="out_button_area">
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <div class="checkout_btn_inner d-flex align-items-center">
                                            <a class="gray_btn" href="#">Continue Shopping</a>
                                            <a class="primary-btn" href="#">Proceed to checkout</a>
                                        </div>
                                    </td>
                                </tr>
                           
                        </tbody>
                    </table>
                </div>
                @else
                    <div class="alert alert-danger">No Items in Cart </div><a href="{{ route('shop.index')}}" style="display:inline-block" class="btn btn-block">Continue Shopping</a>
                @endif
            </div>
        </div>
    </section>
    <section class="saved-for-later">
        <div class="container">
            @if(Cart::instance('savedCart')->count() > 0 )
    <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Product</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(Cart::instance('savedCart')->content() as $item)
                                    <tr>
                                        <td>
                                            <div class="media">
                                                <div class="d-flex">
                                                <a href="{{route('shop.show',$item->model->slug)}}"><img src="img/cart.jpg" alt=""></a>
                                                </div>
                                                <div class="media-body">
                                                    <p> <a href="{{route('shop.show',$item->model->slug)}}">{{$item->model->name }}</a></p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h5>{{$item->model->presentPrice() }}</h5>
                                        </td>
                                        <td>
                                            <div class="product_count">
                                                <input type="text" name="qty" id="sst" maxlength="12" value="1" title="Quantity:"
                                                    class="input-text qty">
                                                <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst )) result.value++;return false;"
                                                    class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
                                                <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;"
                                                    class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button>
                                                    <form  method="POST" action="{{ route('saveForLater.destroy',$item->rowId ) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit">Remove</button>
                                                    </form><br>
                                                    <form  method="POST" action="{{ route('saveForLater.switchToCart',$item->rowId ) }}">
                                                        @csrf
                                                        <button type="submit">Move to cart</button>
                                                    </form>
                                            </div>
                                        </td>
                                        <td>
                                            <h5>{{$item->model->presentPrice() }}</h5>
                                        </td>
                                    </tr>
                                    @endforeach
                           
                        </tbody>
                    </table>
                </div>
                @else
                <div class="alert alert-danger">No Items in saved for later </div>
            
                @endif
                
                </div>
    </section>
    <!--================End Cart Area =================-->
@endsection