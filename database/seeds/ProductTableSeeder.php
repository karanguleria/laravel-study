<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name'=>'Denim Jeans',
            'slug'=>'denim-jeans',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 1',
            'slug'=>'denim-jeans-1',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 2',
            'slug'=>'denim-jeans-2',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 3',
            'slug'=>'denim-jeans-3',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 4',
            'slug'=>'denim-jeans-4',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 5',
            'slug'=>'denim-jeans-5',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 6',
            'slug'=>'denim-jeans-6',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 7',
            'slug'=>'denim-jeans-7',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 8',
            'slug'=>'denim-jeans-8',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);  Product::create([
            'name'=>'Denim Jeans 9',
            'slug'=>'denim-jeans-9',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 10',
            'slug'=>'denim-jeans-10',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);
        Product::create([
            'name'=>'Denim Jeans 11',
            'slug'=>'denim-jeans-11',
            'detail'=>'Faded SkyBlu Denim Jeans',
            'price'=>14999,
            'description'=>'Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.'
        ]);

    }
}
