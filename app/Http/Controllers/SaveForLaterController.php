<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class SaveForLaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToCart($id)
    {
       
    $item = Cart::instance('savedCart')->get($id);
    Cart::instance('savedCart')->remove($id);
    $duplicate = Cart::instance('default')->search(function($cartitem,$rowId) use ($id){
        return $rowId === $id;
    });
    if($duplicate->isNotEmpty()){
        return redirect()->route('cart.index')->with('success_message','Item is already in your cart');
    }
       Cart::instance('default')->add($item->id, $item->name, 1, $item->price)->associate('App\Product');
       return redirect()->route('cart.index')->with('success_message',"Item is successfull switched to cart");
   }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::instance('savedCart')->remove($id);
        return back()->with("success_message","Item Successfully Removed form saved for later Cart");
    }
}
