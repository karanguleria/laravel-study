<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    function presentPrice(){
        return money_format('$ %i',$this->price);
    }
}
