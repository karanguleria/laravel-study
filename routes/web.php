<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','LandingPageController@index')->name('landing-page');
//Route::view('/','landing-page');
Route::get('/shop','ShopController@index')->name('shop.index');
Route::get('/shop/{product}','ShopController@show')->name('shop.show');   

Route::get('/cart','CartController@index')->name('cart.index');   
Route::post('/cart','CartController@store')->name('cart.store');   
Route::delete('/cart/{productId}','CartController@destroy')->name('cart.destroy');   
Route::post('/cart/saveForLater/{productId}','CartController@saveForLater')->name('cart.saveForLater');  
Route::delete('/saveForLater/{productId}','SaveForLaterController@destroy')->name('saveForLater.destroy');   
Route::post('/switchToCart/{productId}','SaveForLaterController@switchToCart')->name('saveForLater.switchToCart');   
// Route::view('/shop','shop');
Route::view('/product','product');
Route::view('/checkout','checkout')->name('checkout');
Route::view('/thankyou','thankyou');
Route::view('/blog','blog');
Route::view('/single-blog','single-blog');
Route::view('/tracking','tracking');
Route::view('/contact','contact');
Route::view('/elements','elements');
Route::view('/login','login');






// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');





Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
