<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Composer\DependencyResolver\Problem;

class LandingPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $products = Product::inRandomOrder()->take(8)->get();
     $dealOfWeek = Product::inRandomOrder()->take(9)->get();
     return view('landing-page')->with([
         'products'=>$products,
     'dealOfWeek'=>$dealOfWeek
     ]);
     
    }
}